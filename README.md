This service requires a server with Docker, it should **NOT** be used on production servers.

## Portainer
Does not need any other container to function, although a [Portainer](portainer.md) can be deployed for easy maintenance.

## Volumes
Each Runner requires some volumes to store their configuration and cache.

If the cache is associated with an external server like S3, it is not necessary to create the volume.

```
docker volume create runner1
docker volume create cache1
```

If the GitLab server has self-signed certificates, it is necessary to copy the CA to the runners so that they can access. 

```
sudo mkdir -p /var/lib/docker/volumes/runner1/_data/certs
sudo mv cert-ca.crt /var/lib/docker/volumes/runner1/_data/certs/ca.crt
```

## Configuration
Each runner requires a configuration file, it is possible to create it manually or use its wizard. This is the process with the wizard.

Run the register.
```
docker run --rm -it -v runner1:/etc/gitlab-runner gitlab/gitlab-runner register
```

Runners can be assigned to 3 levels:
- **Global**: Any project can use it. Configuration in `Admin Area` - `Runner`.
- **Group**: Projects in that group can use it. Configuration in the group, `Settings` - `CI/CD` - `Runners`
- **Project**: Only that project can use it. Configuration in the project, `Settings` - `CI/CD` - `Runners`.

This is an example of the values:
```
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://git.domain.intranet/
Enter the registration token:
token_code
Enter a description for the runner:
[39a6b33c4ded]: Runner1
Enter tags for the runner (comma-separated):
runner1,docker
Registering runner... succeeded                     runner=aaaaaaaa
Enter an executor: kubernetes, docker, shell, docker+machine, docker-ssh+machine, virtualbox, custom, docker-ssh, parallels, ssh:
docker
Enter the default Docker image (for example, ruby:2.6):
docker:stable
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Configuration file requires some changes.
```
sudo vi /var/lib/docker/volumes/runner1/_data/config.toml
```

These changes allow the use of 5 concurrent instances for the same runner and enable the management of the dockers to avoid problems when generating the new containers. 
```
concurrent = 5
volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
```

### Global Runner
Global runners can be configured for any project to use, not only those that are configured with the same tags 

In GitLab, go to
1. Admin Area
2. Runners
3. Edit the Runner
4. Check **Indicates whether this runner can pick jobs without tags**
5. Save changes



## Runner

Start the runner:
```
docker run -d --name runner1 --restart always \
-v runner1:/etc/gitlab-runner \
-v cache1:/mnt/cache \
-v /var/run/docker.sock:/var/run/docker.sock \
gitlab/gitlab-runner:latest
```

