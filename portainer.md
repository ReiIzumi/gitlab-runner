This is a simple deployment to manage a local Docker, without agents. Since it does not have https either, the connection to it is not secure.

```
docker volume create portainer_data
docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```

Access with:
```
http://serverName.domain.intranet:9000/
```